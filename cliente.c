#include "header.h"

void print_usage_message();
void process_response();
void save_file();
void print_dir();

char parameter[BUFF_SIZE];
message pmessage;
file_message pfmessage;
int sfd;
int main(int argc, char* argv[] ) {

	system("clear");

		//------------- petición de una ip y puerto como argumento de entrada ------------------
	printf("\n\n");
	if ( argc != 3) {
		printf("Uso: %s ip port\n", argv[0]);
		exit(1);
	}

	char ip[16];
	char c_port[6];
	int port;
	
	strcpy(c_port, argv[2]);
	if( (port = strtol(argv[2], NULL, 10)) == 0 || port > 65635 || port < 1000) {
		printf("Número de puerto inválido o fuera de rango válido.\n");
		return 1;
	}
	int addr_1, addr_2, addr_3, addr_4;
	if ( sscanf(argv[1], "%d.%d.%d.%d", &addr_1, &addr_2, &addr_3, &addr_4) != 4) {
		printf("IP inválida.\n");
		return 1;
	}
	if ( addr_1 < 0 || addr_2 < 0 || addr_3 < 0 || addr_4 < 0 || addr_1 > 255  || addr_2 > 255 || addr_3 > 255 || addr_4 > 255 ) {
		printf("IP fuera de rango IP V4.\n");
		return 1;
	} 
	
	printf("IP recibida: %d.%d.%d.%d,  PUERTO: %d\n", addr_1, addr_2, addr_3, addr_4, port);
	sprintf(ip,"%d.%d.%d.%d", addr_1, addr_2, addr_3, addr_4);
	
	//--------------- Iniciamos lo necesario para conectarse al servidor ftp --------------
	
	printf("Conectando al servidor ftp...\n");
	
	if( (sfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("Error en creación del socket 1");
		return -1;
	}
	printf("Conectado.\n");
	
	struct sockaddr_in client_addr;
	client_addr.sin_family = AF_INET;
	client_addr.sin_addr.s_addr = inet_addr(ip);
	client_addr.sin_port = htons(port);
	
	//-------------- Inicializar comunicación con el servidor ---------------------
	if ( connect( sfd, (struct sockaddr *) &client_addr, sizeof(client_addr) ) < 0) {
		perror("Error al conectarse al servidor");
		return -1;
	}
	
	
	//-------------- Loop de recepción de comandos ----------------------
	char command[BUFF_SIZE];
	
	char buffer [BUFF_SIZE];
	int numParam;
	system("clear");
	printf("##### My FTP Client #####\n");
	printf("Esperando mensaje inicial del servidor...\n");
	read(sfd, &pmessage, sizeof(pmessage));
	if(pmessage.m_code != HELLO) {
		printf("Error interno, intente más tarde. :'(");
		return -1;
	}
	printf("El servidor dice: ¡hola!.\n");
	directory_message pdmessage;
	while (true) {
			
		printf("s_ftp > ");
		fgets(buffer, BUFF_SIZE, stdin);
		if ( (numParam = sscanf( buffer ,"%s %s", command, parameter) ) == 2 ) {
			//-------- get: obtener archivo remoto --------
			if (strcmp(command, "get") == 0) {
				pmessage.m_code = GETFILE;
				pmessage.m_length = strlen(parameter);
				//Enviando petición y tamaño de ruta de archivo
				write(sfd, &pmessage, sizeof(message));
				//leyendo respuesta de aceptación de transferencia del archivo:
				read(sfd, &pmessage, sizeof(message));
				if (pmessage.m_code == SEND_FILE) {
					//Enviando ruta completa de archivo					
					strcpy(pdmessage.directory_item, parameter);				
					write(sfd, &pdmessage, sizeof(directory_message));
				}
				//leyendo respuesta de acceso al directorio:
				read(sfd, &pmessage, sizeof(message) );
				process_response();
			} 
			
			//-------- list: pedir listado del directorio ---------
			else if (strcmp(command, "list") == 0) {
				pmessage.m_code = LISTDIR;
				pmessage.m_length = strlen(parameter);
				//Enviando petición de listado
				write(sfd, &pmessage, sizeof(message));
				//leyendo respuesta de aceptación de tamaño de la ruta del directorio:
				read(sfd, &pmessage, sizeof(message) );
				if (pmessage.m_code == SEND_DIR) {
					strcpy(pdmessage.directory_item, parameter);				
					write(sfd, &pdmessage, sizeof(directory_message));
				}
				//leyendo respuesta de acceso al directorio:
				read(sfd, &pmessage, sizeof(message) );
				process_response();
			}else {
				print_usage_message();
			}
			
		} else {
			//-------- bye: salir --------
			if (strcmp(command, "bye") == 0) {
				pmessage.m_code = ENDCONN;
				write(sfd, &pmessage, sizeof(pmessage));
				break;
			} else {
				print_usage_message(sfd);
			}
		}
	}		

	close(sfd);
	printf("##### Bye #####\n");
	return 0;
}

void print_usage_message() {
	printf("Comandos válidos: \n\t\"get \\file_route\\filename\" para obtener un archivo del servidor remoto.\n\t\"list \\directory_route\\\": para mostrar el contenido del directorio remoto, no permite el uso de \"..\".\n\t\"bye\": para salir.\n");
}

void process_response() {
//seleccionamos la acción a ejecutar de acuerdo al mensaje recibido del servidor:
	switch (pmessage.m_code) {
			case ACC_DENIED:
				printf("Acceso denegado al recurso.\n");									
				break;
			case FI_NOTFOUND:
				printf("Archivo no encontrado.\n");									
				break;
			case INTERNAL_ERR:
				printf("Error interno\n.");
				exit(-1);					
				break;
			case UNKWN_COMM:
				printf("Comando desconocido.\n");
				exit(-1);				
				break;
			case ROUTE_ISDIR:
				printf("La ruta es un directorio.\n");								
				break;
			case DIR_NOTFOUND:
				printf("Directorio no encontrado.\n");									
				break;
			case ROUTE_NISDIR:
				printf("La ruta proporcionada no es un directorio.\n");									
				break;
			case SEND_DIR:
				printf("\nListado del directorio: \n");
				print_dir();			
				break;
			case SEND_FILE:
				printf("\nGuardando archivo recibido. \n");
				save_file();
				break;		
			default:
				printf("\nRESPUESTA DESCONOCIDA, terminando conexión.\n");
				exit(-1);		
				break;
	}
}

void save_file() {
	unsigned char filepart[MDATA_SIZE];
	file_message pfmessage;
		
	printf("Guardando archivo solicitado...\n");
	
	//Abrimos para escritura el archivo a guardar
	FILE *pFile = NULL;
	char* filename;
	if ((filename = strrchr(parameter, '/')) != NULL) {
		pFile = fopen(filename + 1, "w");
	} else {
		pFile = fopen(parameter, "w");
	}	
	
	if (pFile == NULL)
	{
		printf("Error opening file!\n");
		exit(1);
	}
	
	int written_bytes;
	printf("Leyendo y guardando parte de archivo: ");
	do {
		printf("=");
		read(sfd, &pfmessage, sizeof(file_message));
		
			if ( ( written_bytes = fwrite(pfmessage.data, sizeof(unsigned char), pfmessage.d_length, pFile )) != pfmessage.d_length ) {
				perror("Error de escritura de Archivo: ");
				exit(-1);
			}					
		if(DEBUG)
			printf("=:=%d\n", pfmessage.d_length);
	} while(pfmessage.d_length == MDATA_SIZE);
	fclose(pFile);
	printf(">\n");
	printf("Fín de transmisión.\n");
	
}

void print_dir() {
	printf("Descargando listado de directorio solicitado...\n");
	directory_message pdmessage;
	do {
		read(sfd, &pdmessage, sizeof(directory_message));
		if(pdmessage.last_dir != 'y') {
				printf("%s\n", pdmessage.directory_item);
				if(DEBUG)
					printf("\t=:=%c\n", pdmessage.last_dir);
			}
	} while(pdmessage.last_dir != 'y');
	//printf("%s=:=%c\n", pdmessage.directory_item, pdmessage.last_dir);
	printf("\nFin de directorio.\n");

}

	
