#include "header.h"

void ftp_server_service(int nsfd);

void print_log_message(char* message);
void getFile(int nsfd);
void sendDir(int nsfd);

message pmessage;

int main (int argc, char* argv[]) {
	//------------- petición de una ip y puerto como argumento de entrada ------------------
	printf("\n\n");
	if ( argc != 3) {
		printf("Uso: %s ip port\n", argv[0]);
		exit(1);
	}

	char ip[16];
	char c_port[6];
	int port;
	
	strcpy(c_port, argv[2]);
	if( (port = strtol(argv[2], NULL, 10)) == 0 || port > 65635 || port < 1000) {
		printf("Número de puerto inválido o fuera de rango válido.\n");
		return 1;
	}
	int addr_1, addr_2, addr_3, addr_4;
	if ( sscanf(argv[1], "%d.%d.%d.%d", &addr_1, &addr_2, &addr_3, &addr_4) != 4) {
		printf("IP inválida.\n");
		return 1;
	}
	if ( addr_1 < 0 || addr_2 < 0 || addr_3 < 0 || addr_4 < 0 || addr_1 > 255  || addr_2 > 255 || addr_3 > 255 || addr_4 > 255 ) {
		printf("IP fuera de rango IP V4.\n");
		return 1;
	} 
	
	printf("IP recibida: %d.%d.%d.%d,  PUERTO: %d\n", addr_1, addr_2, addr_3, addr_4, port);
	sprintf(ip,"%d.%d.%d.%d", addr_1, addr_2, addr_3, addr_4);

		

	//------------- Manejo de la ruta base ---------------------------
	
	printf("Servidor preg inicializado, registro de actividades en archivo %s .\n", LOG_ADDR);
	char start_route[PATH_MAX + 1];
	char file_route[PATH_MAX + NAME_MAX +1];

	strcpy(start_route, argv[1]);
	

	//-------------- Redirigir stdout al log file --------------------------------
	int logfd;
	char message[100];
	if ( (logfd = open (LOG_ADDR,  O_RDWR|O_CREAT|O_APPEND, 0600) ) < 0) {
		perror("Error en apertura de archivo log");
		return -1;
	}
	dup2(logfd, STDOUT_FILENO);

	sprintf(message, "\nRuta ingresada: %s", start_route);

	print_log_message(message);
	
	//------------ Preparando estructura de comunicación --------------

	int sfd, nsfd, pid, dir_cli_len;
	struct sockaddr_in dir_ser, dir_cli;

	
	if ((sfd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		perror("Error abriendo socket.");
		return -1;
	}
	dir_ser.sin_family = AF_INET;
	dir_ser.sin_addr.s_addr = inet_addr(ip);
	dir_ser.sin_port = htons(port);

	if (bind(sfd, (struct sockaddr *) &dir_ser,sizeof(dir_ser)) == -1) {
		perror("bind");
		return -1;
	}
	
		
	//------------ Permitir al proceso escuchar intentos de conexión entrantes máximo 5 ------------
	listen(sfd, 5);
	
	//------------ Loop infinito de esperar cliente, aceptar conexión y crear un thread que lo atienda.

	while(true) {
		dir_cli_len = sizeof(dir_cli);		
		print_log_message("\nEsperando conexiones entrantes...");
		if ((nsfd = accept(sfd, (struct sockaddr *) &dir_cli, &dir_cli_len)) == -1) {
			perror("accept");
			return -1;
		}
		if ((pid = fork()) == -1) {
			perror("fork");
			return -1;
		} else if (pid == 0) {
			/* Código del proceso hijo. */
			close(sfd);
			ftp_server_service(nsfd);
			return 0;
		}
		/* Código del proceso padre. */
		close(nsfd);
	}

	printf("\n\n");
	return 0;
}


void ftp_server_service(int nsfd) {
	print_log_message("\nCliente conectado");
	pmessage.m_code = HELLO;
	write(nsfd, &pmessage, sizeof(pmessage));
	do {
		print_log_message("\nEsperando un mensaje entrante.");
		read(nsfd, &pmessage, sizeof(pmessage));
		print_log_message("\nMensaje recibido.");
		
		switch (pmessage.m_code) {
			case GETFILE:
				print_log_message("\nProcesando petición de archivo.");
				//Seguridad: asegurar el tamaño del comando sea del tamaño adecuado.
				if (pmessage.m_length > BUFF_SIZE) {
					print_log_message("\nError interno, notificando a cliente...");
					pmessage.m_code = INTERNAL_ERR;					
					write(nsfd, &pmessage, sizeof(pmessage));
				} else {
									
					getFile(nsfd);
				}					
				break;
			case LISTDIR:
				print_log_message("\nProcesando petición de directorio.");
				//Seguridad: asegurar el tamaño del comando sea del tamaño adecuado.
				if (pmessage.m_length > BUFF_SIZE) {
					print_log_message("\nError interno, notificando a cliente...");
					pmessage.m_code = INTERNAL_ERR;
					write(nsfd, &pmessage, sizeof(pmessage));
				} else {
					sendDir(nsfd);
				}				
				break;				
			case ENDCONN:				
				print_log_message("\nRecibido mensaje de fín de conexión.");				
				break;
			default:
				print_log_message("\nComando desconocido, terminando conexión.");
				pmessage.m_code = UNKWN_COMM;
				write(nsfd, &pmessage, sizeof(pmessage));
				break;

		}		
	} while(pmessage.m_code != ENDCONN && pmessage.m_code != INTERNAL_ERR);
	print_log_message("\nConexión terminada.");
}

void print_log_message(char* message) {
	time_t     now;
	    struct tm ts;
	    char buf[BUFF_SIZE];

	    // Get the current time
	    time(&now);

	    // Format and print the time, "hh:mm:ss "
	    ts = *localtime(&now);
	    strftime(buf, sizeof(buf), "[%H:%M:%S %d/%m/%y]", &ts);
	    printf("%s %s: %s\n", buf, C_ADDR, message);

}

void getFile(int nsfd) {
	char pathname[BUFF_SIZE];
		directory_message pdmessage;
		file_message pfmessage;
		
		//Notificando aceptación de tamaño de ruta de archivo:
		pmessage.m_code = SEND_FILE;
		write(nsfd, &pmessage, sizeof(message));

		print_log_message("\nObteniendo ruta del archivo a enviar...");
		read(nsfd, &pdmessage, sizeof(directory_message));
		strcpy(pathname, pdmessage.directory_item);

		FILE* pFile;
		long lSize;
		
		//Obtenemos la ruta del directorio actual de trabajo del servidor:
		
		if ( strstr(pathname, "..") == 0 && strcmp(pathname,".") != 0 ) {
			char cwd[BUFF_SIZE];
			if (getcwd(cwd, sizeof(cwd)) != NULL) {
				print_log_message("\nDirectorio actual del servidor se obtuvo correctamente.");
			} else {
				perror("getcwd() error");
				pmessage.m_code = INTERNAL_ERR;
				write(nsfd, &pmessage, sizeof(pmessage));
				exit(-1);
			}

			if (pathname[0] != '/') {
				//agregar un / al principio en caso que no lo traiga.
				char aux[BUFF_SIZE];
				strcpy(aux, "/");
				strcpy(pathname, strcat(aux, pathname) );
				
			}
			//Agregamos en CWD del server a la ruta:
			strcpy(pathname, strcat(cwd, pathname));
			if(DEBUG)
				printf("fopen archivo: %s\n", pathname);
			if ((pFile = fopen(pathname, "r")) != 0) {
			
				//Notificación de enviado de archivo: 						
				pmessage.m_code = SEND_FILE;
				write(nsfd, &pmessage, sizeof(pmessage));
				struct dirent* d_entry;
				print_log_message("\nEnviando contenido en partes del archivo requerido: ");
				printf("%s\n",pathname);
				
				// obtain file size:
 			 	fseek (pFile , 0 , SEEK_END);
  				lSize = ftell (pFile);
  				rewind (pFile);
  				
  				printf("File size: %ld bytes\n", lSize); 
  				long sent_bytes = 0;
  				unsigned int read_bytes; 
				while (sent_bytes < lSize || read_bytes == MDATA_SIZE) {				
					print_log_message("");
					printf("Enviando parte %ld de %ld.\n", sent_bytes + 128,lSize);
					//leemos MDATA_SIZE bytes de archivo:
					if ( ( read_bytes = fread(&pfmessage.data, sizeof(unsigned char), MDATA_SIZE , pFile )) != MDATA_SIZE ) {		
					
						//Validamos si han ocurrido errores:
						if (ferror(pFile)) {
							//De haber ocurrido errores informamos al cliente:	
							print_log_message("Error en envío de archivo.\n");										
							break;							
						}
						printf("EOF, remaining bytes: %d \n", read_bytes);
						//EOF	
					}
					//Le mandamos al cliente read_bytes de archivo en cada mensaje: 
					sent_bytes += read_bytes;
					pfmessage.d_length = read_bytes;					
					write(nsfd, &pfmessage, sizeof(pfmessage));
				}
				
			} else {
				switch (errno) {
					case EACCES:
						pmessage.m_code = ACC_DENIED;
						print_log_message("\nACCESO DENEGADO A ARCHIVO REQUERIDO.");
						write(nsfd, &pmessage, sizeof(pmessage));
						break;
					case ENOENT:
						pmessage.m_code = FI_NOTFOUND;
						print_log_message("\nARCHIVO REQUERIDO NO ENCONTRADO.");
						write(nsfd, &pmessage, sizeof(pmessage));
						break;
					case ENOTDIR:
						pmessage.m_code = ROUTE_NISDIR;
						print_log_message("\nDIRECTORIO REQUERIDO NO ES DIRECTORIO!!");
						write(nsfd, &pmessage, sizeof(pmessage));
						break;
					default:
						pmessage.m_code = INTERNAL_ERR;
						print_log_message("\nERROR INTERNO EN ENVIO DE ARCHIVO.");
						write(nsfd, &pmessage, sizeof(pmessage));
						break;
				}			
			}
		} else {
			pmessage.m_code = ACC_DENIED;
			print_log_message("\nACCESO DENEGADO A DIRECTORIO REQUERIDO, USO DE \"..\".");
			write(nsfd, &pmessage, sizeof(pmessage));
		}
	
}
void sendDir(int nsfd) {
		char pathname[BUFF_SIZE];
		directory_message pdmessage;
		//Notificando aceptación de tamaño de ruta de archivo:
		pmessage.m_code = SEND_DIR;
		write(nsfd, &pmessage, sizeof(message));

		print_log_message("\nObteniendo ruta del directorio a desplegar...");
		read(nsfd, &pdmessage, sizeof(directory_message));
		strcpy(pathname, pdmessage.directory_item);

		DIR* dir;
		
		//validamos si es directorio entre otros errores:
		if ( strstr(pathname, "..") == 0) {
			char cwd[BUFF_SIZE];
			if (getcwd(cwd, sizeof(cwd)) != NULL) {
				print_log_message("\nDirectorio actual del servidor se obtuvo correctamente.");
			} else {
				perror("getcwd() error");
				pmessage.m_code = INTERNAL_ERR;
				write(nsfd, &pmessage, sizeof(pmessage));
				exit(-1);
			}

			if (pathname[0] != '/') {
				//sprintf no funcionó para esto :/
				//agregar un / al principio.
				char aux[BUFF_SIZE];
				strcpy(aux, "/");
				strcpy(pathname, strcat(aux, pathname) );
				
			}
			strcpy(pathname, strcat(cwd, pathname));
			strcpy(pathname, strcat(pathname, "/") );

			if ((dir = opendir(pathname)) != 0) {
				//Notificación de enviado de directorio: 						
				pmessage.m_code = SEND_DIR;
				write(nsfd, &pmessage, sizeof(pmessage));
				struct dirent* d_entry;
				print_log_message("\nEnviando listado de contenidos del directorio requerido: ");
				printf("%s\n",pathname);
				while ((d_entry = readdir(dir)) != 0) {			
					if (strcmp(d_entry->d_name, ".") == 0 || strcmp(d_entry->d_name, "..") == 0)
						continue;
					pdmessage.last_dir = 'n';
					strcpy(pdmessage.directory_item , d_entry->d_name);
					if (d_entry->d_type == DT_DIR ) 
						strcat(pdmessage.directory_item, "/");	
					//Le mandamos al cliente registro por registro: 
					write(nsfd, &pdmessage, sizeof(pdmessage));
				}
				pdmessage.last_dir = 'y';
				if(DEBUG)
					printf("MESSAGE: %s,%c\n; ", pdmessage.directory_item, pdmessage.last_dir);
				write(nsfd, &pdmessage, sizeof(pdmessage));
			} else {
				switch (errno) {
					case EACCES:
						pmessage.m_code = ACC_DENIED;
						print_log_message("\nACCESO DENEGADO A DIRECTORIO REQUERIDO.");
						write(nsfd, &pmessage, sizeof(pmessage));
						break;
					case ENOENT:
						pmessage.m_code = DIR_NOTFOUND;
						print_log_message("\nDIRECTORIO REQUERIDO NO ENCONTRADO.");
						write(nsfd, &pmessage, sizeof(pmessage));
						break;
					case ENOTDIR:
						pmessage.m_code = ROUTE_NISDIR;
						print_log_message("\nDIRECTORIO REQUERIDO NO ES DIRECTORIO.");
						write(nsfd, &pmessage, sizeof(pmessage));
						break;
					default:
						pmessage.m_code = INTERNAL_ERR;
						print_log_message("\nERROR INTERNO EN ENVIO DE DIRECTORIO.");
						write(nsfd, &pmessage, sizeof(pmessage));
						break;
				}			
			}
		} else {
			pmessage.m_code = ACC_DENIED;
			print_log_message("\nACCESO DENEGADO A DIRECTORIO REQUERIDO, USO DE \"..\".");
			write(nsfd, &pmessage, sizeof(pmessage));
		}
	
	
}
