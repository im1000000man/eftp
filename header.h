#ifndef HEADER_H
#define HEADER_H

#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
//para structura sockaddr_in
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//para exit

#include <unistd.h>
#include <strings.h>
#include <limits.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <errno.h>
#include <dirent.h>
 
/* Not technically required, but needed on some UNIX distributions */
#include <sys/types.h>
#include <sys/stat.h>



#define S_ADDR		"127.0.0.1"
#define C_ADDR		"127.0.0.1"
#define PORT		5555
#define LOG_ADDR	"ftp_log.txt"
#define BUFF_SIZE	PATH_MAX
//.... Para implementaciones que no soportan PATH_MAX: ..... 
//#define BUFF_SIZE	FILENAME_MAX + NAME_MAX + 1
#define MDATA_SIZE	128

//códigos de mensaje
#define HELLO		1
#define GETFILE		101
#define LISTDIR		102
#define ENDCONN		103

//códigos de respuesta
#define ACC_DENIED	201
#define FI_NOTFOUND	202
#define INTERNAL_ERR	203
#define UNKWN_COMM	204
#define ROUTE_ISDIR	205
#define DIR_NOTFOUND	206
#define	ROUTE_NISDIR	207
#define SEND_FILE	301
#define SEND_DIR	302


#define DEBUG 0 

#define true 	1

typedef struct {

		int m_length;
		short int m_code;
	} message;
typedef struct {
		int d_length;
		unsigned char data[MDATA_SIZE];		
	} file_message;
typedef struct {
		char last_dir;
		char directory_item[PATH_MAX];		
	} directory_message;


char fileRoute[BUFF_SIZE];

#endif
